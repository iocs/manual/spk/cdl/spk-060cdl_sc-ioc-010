##############################################################################
# Type:     Cryo Systems
# Project:  CMDS
# DevType:  CMDS-:CRYO: DIAG - Diganostics
# Author:	Adalberto Fontoura, Wojciech Bińczyk
# Date:		29-08-2021
# Version:  v1.0
##############################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()

############################
#  Connected
############################ 

#Local modules connection state
add_digital("PS_Connected",                          PV_DESC="LM Siemens PS connected",           PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("PB_Connected",                          PV_DESC="Profibus Devices connected",        PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("PN_Connected",                          PV_DESC="Profinet Devices connected",        PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("CPU_Connected",                         PV_DESC="LM Siemens CPU connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF2_Connected",                         PV_DESC="LM Siemens KF3 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF3_Connected",                         PV_DESC="LM Siemens KF3 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF4_Connected",                         PV_DESC="LM Siemens KF4 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF5_Connected",                         PV_DESC="LM Siemens KF5 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF6_Connected",                         PV_DESC="LM Siemens KF6 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF7_Connected",                         PV_DESC="LM Siemens KF7 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF8_Connected",                         PV_DESC="LM Siemens KF8 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF9_Connected",                         PV_DESC="LM Siemens KF9 connected",          PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF10_Connected",                        PV_DESC="LM Siemens KF10 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF11_Connected",                        PV_DESC="LM Siemens KF11 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("KF12_Connected",                        PV_DESC="LM Siemens KF12 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected") 
add_digital("KF13_Connected",                        PV_DESC="LM Siemens KF12 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected") 
add_digital("KF14_Connected",                        PV_DESC="LM Siemens KF12 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected") 
add_digital("KF15_Connected",                        PV_DESC="LM Siemens KF12 connected",         PV_ONAM="Connected",               PV_ZNAM="Disconnected") 

#RIO1 connection state
add_digital("RIO1_Connected",                        PV_DESC="RIO1 connected",                    PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF2_Connected",                    PV_DESC="RIO1 KF2 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF3_Connected",                    PV_DESC="RIO1 KF3 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF4_Connected",                    PV_DESC="RIO1 KF4 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF5_Connected",                    PV_DESC="RIO1 KF5 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("RIO1_KF6_Connected",                    PV_DESC="RIO1 KF6 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF7_Connected",                    PV_DESC="RIO1 KF7 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("RIO1_KF8_Connected",                    PV_DESC="RIO1 KF8 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF9_Connected",                    PV_DESC="RIO1 KF9 connected",                PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("RIO1_KF10_Connected",                   PV_DESC="RIO1 KF10 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF11_Connected",                   PV_DESC="RIO1 KF11 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("RIO1_KF12_Connected",                   PV_DESC="RIO1 KF12 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF13_Connected",                   PV_DESC="RIO1 KF13 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")    
add_digital("RIO1_KF14_Connected",                   PV_DESC="RIO1 KF14 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("RIO1_KF15_Connected",                   PV_DESC="RIO1 KF15 connected",               PV_ONAM="Connected",               PV_ZNAM="Disconnected")  

#Lakeshore connection state
add_digital("LKSR01_Connected",						 PV_DESC="Lakeshore 01 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")  
add_digital("LKSR02_Connected",						 PV_DESC="Lakeshore 02 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR03_Connected",						 PV_DESC="Lakeshore 03 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR04_Connected",						 PV_DESC="Lakeshore 04 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR05_Connected",						 PV_DESC="Lakeshore 05 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR06_Connected",						 PV_DESC="Lakeshore 06 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR07_Connected",						 PV_DESC="Lakeshore 07 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR08_Connected",						 PV_DESC="Lakeshore 08 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR09_Connected",						 PV_DESC="Lakeshore 09 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LKSR10_Connected",						 PV_DESC="Lakeshore 10 connected",            PV_ONAM="Connected",               PV_ZNAM="Disconnected")

#CV connection state
add_digital("CV01_Connected",                        PV_DESC="CV01 connected",                    PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("CV02_Connected",						 PV_DESC="CV02 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("CV03_Connected",						 PV_DESC="CV03 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV04_Connected",						 PV_DESC="CV04 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV05_Connected",						 PV_DESC="CV05 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV06_Connected",						 PV_DESC="CV06 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV60_Connected",						 PV_DESC="CV60 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV61_Connected",						 PV_DESC="CV61 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV62_Connected",						 PV_DESC="CV62 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV63_Connected",						 PV_DESC="CV63 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV91_Connected",						 PV_DESC="CV91 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV92_Connected",						 PV_DESC="CV92 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV93_Connected",						 PV_DESC="CV93 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("CV94_Connected",						 PV_DESC="CV94 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")


#TDK Lambda TA Power supply state
add_digital("TA11_Connected",                        PV_DESC="TA11 connected",                    PV_ONAM="Connected",               PV_ZNAM="Disconnected") 
add_digital("TA12_Connected",				         PV_DESC="TA12 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected") 
add_digital("TA13_Connected",				         PV_DESC="TA13 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA14_Connected",				         PV_DESC="TA14 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA15_Connected",				         PV_DESC="TA15 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA16_Connected",				         PV_DESC="TA16 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA17_Connected",				         PV_DESC="TA17 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA18_Connected",				         PV_DESC="TA18 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA19_Connected",				         PV_DESC="TA19 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA20_Connected",				         PV_DESC="TA20 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA21_Connected",				         PV_DESC="TA21 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("TA22_Connected",				         PV_DESC="TA22 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")

#Level Controller State
add_digital("LSC01_Connected",						 PV_DESC="LC01 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")
add_digital("LSC02_Connected",						 PV_DESC="LC02 connected",					  PV_ONAM="Connected",               PV_ZNAM="Disconnected")


#ClipX connection state
add_digital("CPx1_Connected",                        PV_DESC="CLIPX01 connected",                 PV_ONAM="Connected",               PV_ZNAM="Disconnected")      
add_digital("CPx2_Connected",					     PV_DESC="CLIPX02 connected",			      PV_ONAM="Connected",               PV_ZNAM="Disconnected")    


############################
#  Error Status
############################ 

#Local modules error state
add_digital("PS_Status",                             PV_DESC="LM Siemens PS state",               PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("CPU_Status",                            PV_DESC="LM Siemens CPU state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("PB_Status",                             PV_DESC="Profibus Devices state",            PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("PN_Status",                             PV_DESC="Profinet Devices state",            PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF2_Status",                            PV_DESC="LM Siemens KF3 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF3_Status",                            PV_DESC="LM Siemens KF3 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF4_Status",                            PV_DESC="LM Siemens KF4 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF5_Status",                            PV_DESC="LM Siemens KF5 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF6_Status",                            PV_DESC="LM Siemens KF6 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF7_Status",                            PV_DESC="LM Siemens KF7 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF8_Status",                            PV_DESC="LM Siemens KF8 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF9_Status",                            PV_DESC="LM Siemens KF9 state",              PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF10_Status",                           PV_DESC="LM Siemens KF10 state",             PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF11_Status",                           PV_DESC="LM Siemens KF11 state",             PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("KF12_Status",                           PV_DESC="LM Siemens KF12 state",             PV_ONAM="OK",                      PV_ZNAM="NOK") 
add_digital("KF13_Status",                           PV_DESC="LM Siemens KF12 state",             PV_ONAM="OK",                      PV_ZNAM="NOK") 
add_digital("KF14_Status",                           PV_DESC="LM Siemens KF12 state",             PV_ONAM="OK",                      PV_ZNAM="NOK") 
add_digital("KF15_Status",                           PV_DESC="LM Siemens KF12 state",             PV_ONAM="OK",                      PV_ZNAM="NOK") 

#RIO1 error state
add_digital("RIO1_Status",                           PV_DESC="RIO1 state",                        PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF2_Status",                       PV_DESC="RIO1 KF2 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF3_Status",                       PV_DESC="RIO1 KF3 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF4_Status",                       PV_DESC="RIO1 KF4 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF5_Status",                       PV_DESC="RIO1 KF5 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("RIO1_KF6_Status",                       PV_DESC="RIO1 KF6 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF7_Status",                       PV_DESC="RIO1 KF7 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("RIO1_KF8_Status",                       PV_DESC="RIO1 KF8 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF9_Status",                       PV_DESC="RIO1 KF9 state",                    PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("RIO1_KF10_Status",                      PV_DESC="RIO1 KF10 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF11_Status",                      PV_DESC="RIO1 KF11 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("RIO1_KF12_Status",                      PV_DESC="RIO1 KF12 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF13_Status",                      PV_DESC="RIO1 KF13 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")    
add_digital("RIO1_KF14_Status",                      PV_DESC="RIO1 KF14 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("RIO1_KF15_Status",                      PV_DESC="RIO1 KF15 state",                   PV_ONAM="OK",                      PV_ZNAM="NOK")  

#Lakeshore error state
add_digital("LKSR01_Status",						 PV_DESC="Lakeshore 01 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")  
add_digital("LKSR02_Status",						 PV_DESC="Lakeshore 02 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR03_Status",						 PV_DESC="Lakeshore 03 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR04_Status",						 PV_DESC="Lakeshore 04 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR05_Status",						 PV_DESC="Lakeshore 05 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR06_Status",						 PV_DESC="Lakeshore 06 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR07_Status",						 PV_DESC="Lakeshore 07 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR08_Status",						 PV_DESC="Lakeshore 08 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR09_Status",						 PV_DESC="Lakeshore 09 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LKSR10_Status",						 PV_DESC="Lakeshore 10 state",                PV_ONAM="OK",                      PV_ZNAM="NOK")

#CV error state
add_digital("CV01_Status",                           PV_DESC="CV01 state",                        PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("CV02_Status",							 PV_DESC="CV02 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("CV03_Status",							 PV_DESC="CV03 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV04_Status",							 PV_DESC="CV04 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV05_Status",							 PV_DESC="CV05 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV06_Status",							 PV_DESC="CV06 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV60_Status",							 PV_DESC="CV60 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV61_Status",							 PV_DESC="CV61 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV62_Status",							 PV_DESC="CV62 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV63_Status",							 PV_DESC="CV63 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV91_Status",							 PV_DESC="CV63 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV92_Status",							 PV_DESC="CV92 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV93_Status",							 PV_DESC="CV93 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("CV94_Status",							 PV_DESC="CV94 state",					      PV_ONAM="OK",                      PV_ZNAM="NOK")


#TDK Lambda TA Power supply state
add_digital("TA11_Status",                           PV_DESC="TA11 Status",                       PV_ONAM="OK",                      PV_ZNAM="NOK") 
add_digital("TA12_Status",						     PV_DESC="TA12 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK") 
add_digital("TA13_Status",						     PV_DESC="TA13 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA14_Status",						     PV_DESC="TA14 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA15_Status",						     PV_DESC="TA15 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA16_Status",						     PV_DESC="TA16 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA17_Status",						     PV_DESC="TA17 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA18_Status",						     PV_DESC="TA18 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA19_Status",						     PV_DESC="TA19 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA20_Status",						     PV_DESC="TA20 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA21_Status",						     PV_DESC="TA21 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("TA22_Status",						     PV_DESC="TA22 Status", 					  PV_ONAM="OK",                      PV_ZNAM="NOK")

#Level Controller State
add_digital("LSC01_Status",						     PV_DESC="LC01 Status",					      PV_ONAM="OK",                      PV_ZNAM="NOK")
add_digital("LSC02_Status",	                         PV_DESC="LC02 Status",					      PV_ONAM="OK",                      PV_ZNAM="NOK")


#ClipX error state
add_digital("CPx1_Status",                           PV_DESC="CLIPX01 state",                     PV_ONAM="OK",                      PV_ZNAM="NOK")      
add_digital("CPx2_Status",					         PV_DESC="CLIPX02 state",			          PV_ONAM="OK",                      PV_ZNAM="NOK")    

############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("Cmd_AckAlarm",                          PV_DESC="CMD: Acknowledge Alarm")


