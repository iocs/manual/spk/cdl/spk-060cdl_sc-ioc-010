###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ##
##  CCDB device types: ICS_xxxxx                                                            ##
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##
##                          LT- Level Transmitter in percent                                ##
##                                                                                          ##
##                                                                                          ##
############################         Version: 1.6            ################################
# Author:  Wojciech Binczyk
# Date:    08-05-2023
# Version: v1.6
# Changes:
# 1. PVs: Underrange, Overrange, HIHI, LOLO, Module_Error, Param_Error added to archiving.
# 2. Fixed discription Temperature -> Level 
############################         Version: 1.5            ################################
# Author:  Dominik Domagala
# Date:    26-01-2023
# Version: v1.5
# Changes:
# 1. Added pvs P_ScaleHIGH and P_ScaleLOW for control scaling from OPI
############################         Version: 1.4             ################################
# Author:  Emilio Asensi
# Date:    06-09-2022
# Version: v1.4
# Changes:
# 1. Added pvs for timer, filter and ramping speed
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.3
# Changes:
# 1. Variable Name Unification
############################         Version: 1.2             ################################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes:
# 1. Major review,
# 2. Indent,  unit standardization
############################         Version: 1.1             ################################
# Author:  Miklos Boros, Marino Vojneski
# Date:    12-06-2018
# Version: v1.1
# Changes:
# 1. Modified Alarm Signal section to be compatible with new format.
############################ Version: 1.0             ########################################
# Author:  Miklos Boros
# Date:    25-01-2018
# Version: v1.0



############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                       PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",              PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",               PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",        PV_ONAM="InhibitLocking",             PV_ZNAM="AllowLocking")
add_analog("TransmitterColor",         "INT",                            PV_DESC="Transmitter color")

#for OPI visualization
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                       PV_ZNAM="False")
add_analog("ScaleLOW",                 "REAL",  PV_PREC="1",                            PV_DESC="Scale LOW",                  PV_EGU="%")
add_analog("ScaleHIGH",                "REAL",  PV_PREC="1",                            PV_DESC="Scale HIGH",                 PV_EGU="%")

#Transmitter value
add_analog("MeasValue",                "REAL",  PV_PREC="1",   ARCHIVE=" 1Hz",                           PV_DESC="Level Value",          PV_EGU="%")
add_analog("RAWValue",                 "REAL",  PV_PREC="1",   ARCHIVE=" 1Hz",                           PV_DESC="RAW integer scaled" )
add_analog("OffsetValue",              "REAL",  ARCHIVE=True,            PV_DESC="Offset Value",          PV_EGU="%")

add_digital("AlarmRelay",               ARCHIVE=True, PV_DESC="Alarm Status Relay",   PV_ONAM="True",   PV_ZNAM="False")
add_digital("LCMeasS",                 ARCHIVE=True, PV_DESC="Start LC Measurement", PV_ONAM="True",   PV_ZNAM="False")

#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",          PV_ONAM="True",                       PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                           PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                           PV_DESC="Guest Lock ID")

#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",             PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ZNAM="NominalState")
add_major_alarm("Underrange",          "Level Underrange",               ARCHIVE=True,          PV_DESC="Level Underrange",     PV_ZNAM="NominalState")
add_major_alarm("Overrange",           "Level Overrange",                ARCHIVE=True,          PV_DESC="Level Overrange",      PV_ZNAM="NominalState")
add_major_alarm("HIHI",                "Level HIHI",                     ARCHIVE=True,          PV_DESC="Level HIHI",           PV_ZNAM="NominalState")
add_minor_alarm("HI",                  "Level HI",                                                                                PV_ZNAM="NominalState")
add_minor_alarm("LO",                  "Level LO",                                                                                PV_ZNAM="NominalState")
add_major_alarm("LOLO",                "Level LOLO",                     ARCHIVE=True,          PV_DESC="Level LOLO",           PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO_Error",                                                                                PV_ZNAM="NominalState")
add_major_alarm("Module_Error",        "Module_Error",                   ARCHIVE=True,          PV_DESC="Module_Error",         PV_ZNAM="NominalState")
add_major_alarm("Param_Error",         "Parameter_Error",                ARCHIVE=True,          PV_DESC="Parameter_Error",      PV_ZNAM="NominalState")
add_major_alarm("Warm_Sensor",         "Warm Sensor",                    ARCHIVE=True,          PV_DESC="Warm_Sensor",          PV_ZNAM="NominalState")

#Feedback
add_analog("FB_ForceValue",            "REAL",  PV_PREC="1",             ARCHIVE=True,          PV_DESC="Feedback Force Level", PV_EGU="%")
add_analog("FB_Limit_HIHI",            "REAL",  PV_PREC="1",             ARCHIVE=True,          PV_DESC="Feedback Limit HIHI",        PV_EGU="%")
add_analog("FB_Limit_HI",              "REAL",  PV_PREC="1",             ARCHIVE=True,          PV_DESC="Feedback Limit HI",          PV_EGU="%")
add_analog("FB_Limit_LO",              "REAL",  PV_PREC="1",             ARCHIVE=True,          PV_DESC="Feedback Limit LO",          PV_EGU="%")
add_analog("FB_Limit_LOLO",            "REAL",  PV_PREC="1",             ARCHIVE=True,          PV_DESC="Feedback Limit LOLO",        PV_EGU="%")

#Timers and Filters
add_digital("Timer_enabled")
add_digital("T_triggered")
add_analog("T_running", "INT")
add_digital("F_enabled")
add_analog("FiltedValue", "REAL")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceVal",            PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

add_digital("StatRLHe",				   PV_DESC="Status LHe Sensor")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#Limits
add_analog("P_Limit_HIHI",             "REAL",  PV_PREC="1",                            PV_DESC="Limit HIHI",                 PV_EGU="%")
add_analog("P_Limit_HI",               "REAL",  PV_PREC="1",                            PV_DESC="Limit HI",                   PV_EGU="%")
add_analog("P_Limit_LO",               "REAL",  PV_PREC="1",                            PV_DESC="Limit LO",                   PV_EGU="%")
add_analog("P_Limit_LOLO",             "REAL",  PV_PREC="1",                            PV_DESC="Limit LOLO",                 PV_EGU="%")
add_analog("P_ScaleHIGH",              "REAL",  PV_PREC="1",                            PV_DESC="Scaling HIGH",               PV_EGU="%")
add_analog("P_ScaleLOW",               "REAL",  PV_PREC="1",                            PV_DESC="Scaling LOW",                PV_EGU="%")

#Forcing
add_analog("P_ForceValue",             "REAL",  PV_PREC="1",                            PV_DESC="Force Level",                PV_EGU="%")

#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                           PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                           PV_DESC="Device ID after Blockicon Open")

#Timers and Filters
add_analog("T_period", "INT")
add_analog("F_interval", "INT")
add_analog("F_buffer", "INT")
add_digital("F_Enable")
add_analog("P_RampUPSpeed","REAL",  PV_PREC="1",                    PV_DESC="Ramping UP Speed",                              PV_EGU="%/s")
add_analog("P_RampDNSpeed","REAL",  PV_PREC="1",                    PV_DESC="Ramping DOWN Speed",                            PV_EGU="%/s")
add_analog("P_OffsetValue","REAL",  PV_PREC="1",                    PV_DESC="Offset Value",                                  PV_EGU="%")

#Direct Level contrller reading
add_analog("LC_LvlR",      "REAL",     PV_DESC="Read IOC Cont Level",              PV_EGU="%")

#add_verbatim("""
#
#record(dfanout, "[PLCF#INSTALLATION_SLOT]:#StatR"){
#field(SCAN, ".1 second")
#field(DESC, "Check LT status")
#field(OMSL, "closed_loop")
#field(DOL, "[PLCF#INSTALLATION_SLOT]:LHeSensStatR CP")
#field(DISS, "INVALID")
#field(DISA, "1")
#field(DISV, "0")
#field(OUTA,  "[PLCF#INSTALLATION_SLOT]:StatRLHe PP MSS")
#}
#""")
#
#add_verbatim("""
#
#record(dfanout, "[PLCF#INSTALLATION_SLOT]:#LC_LvlR"){
#field(SCAN, ".1 second")
#field(DESC, "Read IOC Cont Level")
#field(OMSL, "closed_loop")
#field(DOL, "[PLCF#INSTALLATION_SLOT]:LvlR CP")
#field(DISS, "INVALID")
#field(DISA, "1")
#field(DISV, "0")
#field(OUTA,  "[PLCF#INSTALLATION_SLOT]:LC_LvlR PP MSS")
#}
#""")
#
#add_verbatim("""
#
#record(dfanout, "[PLCF#INSTALLATION_SLOT]:#LC_MeasS"){
#field(SCAN, ".1 second")
#field(DESC, "Start LC Measurement")
#field(OMSL, "closed_loop")
#field(DOL, "[PLCF#INSTALLATION_SLOT]:LCMeasS CP")
#field(DISS, "INVALID")
#field(DISA, "1")
#field(DISV, "0")
#field(OUTA,  "[PLCF#INSTALLATION_SLOT]:MeasS PP MSS")
#}
#""")
